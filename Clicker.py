# Libraries
from ahk import AHK
import time

# Initalialize
ahk = AHK(executable_path='C:\Program Files\AutoHotkey\AutoHotkey.exe')

# Check if BYOD 
win = ahk.win_get(title='Roycemore-BYOD')
if win.id == '':
    exit()

# Only works with full-screen windows
ahk.mouse_position = (958, 299)
ahk.click()
time.sleep(3)
ahk.mouse_position = (864, 530)
ahk.click()
