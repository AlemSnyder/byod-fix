﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.


settimer, gui, -2000

CheckNet:

runwait, ping -n 10 www.google.com
if errorlevel
	msgbox, Off-Line
else
	msgbox, Online

return

gui:
gui, add, text, , Close cmd windows from "x" button `n (will always show up Off-Line message Box - Nice!)
gui, add, text, , kill "ping.exe" through task manager `n (will always show up Off-Line message Box - Nice!)
gui, add, text, , F11 = run, taskkill /f /im ping.exe = kill "ping.exe" with cmd command `n (will always show up Off-Line message Box - Nice!)
gui, add, text, , F12 = process, close, ping.exe = kill "ping.exe" with AutoHotkey `n (will always show up Online message Box - Not good!)
gui, add, button, xm gCheckNet, Ping Again
gui, show, ,  Process Closer

return

f11::
run, taskkill /f /im ping.exe
return

f12::
process, close, ping.exe
return

guiclose:
exitapp
